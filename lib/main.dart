import 'package:flutter/material.dart';
import 'package:untitled1/utils/explore.dart';
import 'package:untitled1/utils/planner.dart';
import 'package:untitled1/utils/profile.dart';
import 'package:untitled1/utils/programs.dart';
import 'package:untitled1/utils/shoppinglist.dart';

/// Flutter code sample for [BottomNavigationBar].

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: SampleClass(),
    );
  }
}

class SampleClass extends StatefulWidget {
  const SampleClass({super.key});

  @override
  State<SampleClass> createState() =>
      _SampleClassState();
}

class _SampleClassState
    extends State<SampleClass> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> pages = [
    Planner(),
    Programs(),
    Explore(),
    ShoppingList(),
    Profile(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: pages.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.calendar_month
              ),
              label: " Planner",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                  Icons.stars
              ),
              label: "Programs",),
            BottomNavigationBarItem(
              icon: Icon(Icons.assistant_navigation),
              label: " Explore",),
            BottomNavigationBarItem(
              icon: Icon(Icons.list_alt),
              label: "Shopping List",),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              label: "Profile",),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.black,
          unselectedItemColor: Colors.grey.shade400,
          showUnselectedLabels: true,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
