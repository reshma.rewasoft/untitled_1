import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Planner extends StatefulWidget {
  const Planner({super.key});

  @override
  State<Planner> createState() => _PlannerState();
}

class _PlannerState extends State<Planner> {
  double _currentValue=0;
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(backgroundColor: Colors.white,
        title: const Text("Planner",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: 20),),
      ),
      body:Padding(
        padding: const EdgeInsets.only(left:10.0,right: 10),
        child: ListView(
          scrollDirection: Axis.vertical,
          children:  [
            const SizedBox(
              height: 20,
            ),
            const Text('In Progress',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
            const SizedBox(
              height: 10,
            ),

            Card(
              elevation: 5,
              child: Row(
                //  scrollDirection: Axis.horizontal,
                children: [
                  const Image(fit: BoxFit.fill,image: AssetImage('assets/images/fita.jpg'),height: 150,width: 150, ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      const Text('In Progress value',style: TextStyle(fontWeight: FontWeight.w500,fontSize: 20),),
                      const SizedBox(height: 50,),
                      Slider(
                          min: 0.0,
                          max: 8.0,
                          activeColor: Colors.black,
                          inactiveColor: Colors.grey,
                          value: _currentValue, onChanged: (value){
                        setState(() {
                          _currentValue = value;
                        });
                      }),

                    ],
                  ),

                ],
              ),

            ),
            const SizedBox(
              height: 30,
            ),

            const Text('Recommended For You',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),

            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),

              ),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Image(image: AssetImage("assets/images/fita.jpg"),fit: BoxFit.fill,),
                    const SizedBox(height: 10,),
                    Row(
                      children: [
                        SizedBox(
                          height: 40,
                          width: 80,
                          child:
                          Card(
                            color: Colors.grey.shade100,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: const Center(child: Text('HEART')),
                          ),
                        ),
                        SizedBox(
                          height: 40,
                          width: 100,
                          child:
                          Card(
                            color: Colors.grey.shade100,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: const Center(child: Text('STRENGTH')),
                          ),
                        ),
                      ],
                    ),
                    const Text('Program Title',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
                    Text('This is text description',style: TextStyle(color: Colors.grey.shade500),),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('6 WEEK',style: TextStyle(color: Colors.grey.shade500),),
                        const SizedBox(width: 15,),
                        Row(
                          children: [
                            Icon(Icons.circle,size: 5,color: Colors.grey.shade500,),
                            const SizedBox(width: 5,),
                            Text('DUMBLES',style: TextStyle(color: Colors.grey.shade500),),
                          ],
                        ),

                      ],
                    ),

                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text('Functional Movements',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
            const Text('Functional Movements to test',style: TextStyle(fontSize: 15,color: Colors.grey),),
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),

              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Image(image: AssetImage("assets/images/fita.jpg"),fit: BoxFit.fitWidth,),
                    Row(
                      children: [
                        SizedBox(
                          height: 40,
                          width: 80,
                          child:
                          Card(
                            color: Colors.grey.shade100,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: const Center(child: Text('HEART')),
                          ),
                        ),
                        SizedBox(
                          height: 40,
                          width: 100,
                          child:
                          Card(
                            color: Colors.grey.shade100,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: const Center(child: Text('STRENGTH')),
                          ),
                        ),
                      ],
                    ),
                    const Text('Program Title',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
                    Text('This is text description',style: TextStyle(color: Colors.grey.shade500),),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('6 WEEK',style: TextStyle(color: Colors.grey.shade500),),
                        const SizedBox(width: 10,),
                        Row(
                          children: [
                            Icon(Icons.circle,size: 5,color: Colors.grey.shade500,),
                            const SizedBox(width: 5,),
                            Text('DUMBLES',style: TextStyle(color: Colors.grey.shade500),),
                          ],
                        ),

                      ],
                    ),

                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),

            const Text('Recommended For You',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),

            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),

              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Image(image: AssetImage("assets/images/fita.jpg"),fit: BoxFit.fitWidth,),
                    const SizedBox(width: 10,),

                    Row(
                      children: [
                        SizedBox(
                          height: 40,
                          width: 80,
                          child:
                          Card(
                            color: Colors.grey.shade100,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: const Center(child: Text('HEART')),
                          ),
                        ),
                        SizedBox(
                          height: 40,
                          width: 100,
                          child:
                          Card(
                            color: Colors.grey.shade100,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: const Center(child: Text('STRENGTH')),
                          ),
                        ),
                      ],
                    ),
                    const Text('Program Title',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
                    Text('This is text description',style: TextStyle(color: Colors.grey.shade500),),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('6 WEEK',style: TextStyle(color: Colors.grey.shade500),),
                        const SizedBox(width: 10,),
                        Row(
                          children: [
                            Icon(Icons.circle,size: 5,color: Colors.grey.shade500,),
                            const SizedBox(width: 5,),
                            Text('DUMBLES',style: TextStyle(color: Colors.grey.shade500),),
                          ],
                        ),

                      ],
                    ),

                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),

            const Text('Recommended For You',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),

            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),

              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Image(image: AssetImage("assets/images/fita.jpg"),fit: BoxFit.fitWidth,),
                    const SizedBox(width: 10,),

                    Row(
                      children: [
                        SizedBox(
                          height: 40,
                          width: 80,
                          child:
                          Card(
                            color: Colors.grey.shade100,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: const Center(child: Text('HEART')),
                          ),
                        ),
                        SizedBox(
                          height: 40,
                          width: 100,
                          child:
                          Card(
                            color: Colors.grey.shade100,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: const Center(child: Text('STRENGTH')),
                          ),
                        ),
                      ],
                    ),
                    const Text('Program Title',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 30),),
                    Text('This is text description',style: TextStyle(color: Colors.grey.shade500),),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text('6 WEEK',style: TextStyle(color: Colors.grey.shade500),),
                        const SizedBox(width: 10,),
                        Row(
                          children: [
                            Icon(Icons.circle,size: 5,color: Colors.grey.shade500,),
                            const SizedBox(width: 5,),
                            Text('DUMBLES',style: TextStyle(color: Colors.grey.shade500),),
                          ],
                        ),

                      ],
                    ),

                  ],
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
